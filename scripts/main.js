/*Humburger*/
let menuBtn = document.querySelector('.menu-btn');
let menu = document.querySelector('.menu');

menuBtn.addEventListener('click', function(){
	menuBtn.classList.toggle('active');
	menu.classList.toggle('active');
})
/*Scroll*/
window.onscroll = () => {
	const header = document.querySelector('.header');
const Y = window.scrollY
if(Y > 250){
	header.classList.add('animated_menu')
}
else{
	header.classList.remove('animated_menu')
}
};

/*Validation*/
var form = document.querySelector('.right_side')
var validateBtn = form.querySelector('.get_btn')
var firstName = form.querySelector('.First_name')
var email = form.querySelector('.Email')
var Number = form.querySelector('.Number')
var ask_want = form.querySelector('.ask_want')
var fields = form.querySelectorAll('.field')
var generateError = function(text){
	var error = document.createElement('div')
	error.className = 'error'
	error.style.color = 'red'
	error.innerHTML = text
	return error
}

form.addEventListener ('click', function (event){
	event.preventDefault()
var errors = form.querySelectorAll('.error')

for( var i = 0 ; i < errors.length; i++){
	errors[i].remove()
}

for( var i = 0; i < fields.length; i++){
	if(!fields[i].value){
		console.log('Не заполнено поле',fields[i])
 var error = generateError('Не заполнено поле')
		fields[i].parentElement.insertBefore(error, fields[i])
	}
}


})